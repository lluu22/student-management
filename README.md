# StudentManagement

#### 介绍
This is an example project for the course.

#### 安装教程

1.  参考 [Postgres文档](https://www.runoob.com/postgresql/mac-install-postgresql.html) ，在主机上安装Postgres。
2.  创建名为student-management的数据库，用户名和密码均为test。
    （如安装过docker，也可以在docker中启动Postgres，启动命令如下：）
    ```
    docker run --name student-management-postgres -e POSTGRES_USER=test -e POSTGRES_PASSWORD=test -e POSTGRES_DB=student-management -p 5432:5432 -d postgres
    
3.  前往 [代码库](https://gitee.com/guangjie_zhang/student-management) ，克隆此项目到本地并创建gitee分支，确保能够用intelliJ并成功build。

#### 使用说明

1.  build项目：``` ./gradlew clean build```
2.  运行项目： ```./gradlew bootRun```

#### 提交作业

1.  Clone本仓库
2.  新建 Group_xxx 分支
3.  提交代码

